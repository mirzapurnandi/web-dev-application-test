<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\RoomControllers;
use App\Http\Controllers\StudentControllers;
use App\Http\Controllers\TeacherControllers;

//use App\Http\Controllers\TeacherControllers;

Route::group([
    'middleware' => ['auth']
], function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');

    Route::get('/rooms', [RoomControllers::class, 'index'])->name('room.index');
    Route::post('/rooms-store', [RoomControllers::class, 'store'])->name('room.store');
    Route::get('/rooms-getdata/{id}', [RoomControllers::class, 'getdata'])->name('room.getdata');
    Route::post('/rooms-destroy', [RoomControllers::class, 'destroy'])->name('room.destroy');
    Route::get('/rooms-view/{id}', [RoomControllers::class, 'view'])->name('room.view');
    Route::post('/rooms-assign', [RoomControllers::class, 'assign'])->name('room.assign');
    Route::get('/rooms-downloads', [RoomControllers::class, 'downloads'])->name('room.downloads');
    Route::get('/rooms-download/{id}', [RoomControllers::class, 'download'])->name('room.download');

    Route::get('/teachers', [TeacherControllers::class, 'index'])->name('teacher.index');
    Route::post('/teachers-store', [TeacherControllers::class, 'store'])->name('teacher.store');
    Route::get('/teachers-getdata/{id}', [TeacherControllers::class, 'getdata'])->name('teacher.getdata');
    Route::post('/teachers-destroy', [TeacherControllers::class, 'destroy'])->name('teacher.destroy');

    Route::get('/students', [StudentControllers::class, 'index'])->name('student.index');
    Route::post('/students-store', [StudentControllers::class, 'store'])->name('student.store');
    Route::get('/students-getdata/{id}', [StudentControllers::class, 'getdata'])->name('student.getdata');
    Route::post('/students-destroy', [StudentControllers::class, 'destroy'])->name('student.destroy');
});

Route::get('/logout', function () {
    Auth::logout();
    return Redirect::to('login');
});
