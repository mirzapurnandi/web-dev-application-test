<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;

class TeacherControllers extends Controller
{
    public function index()
    {
        $teachers = Teacher::paginate(10);

        return view('teacher.index', ['teachers' => $teachers]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string'
        ]);

        $arr_data = [
            'name' => $request->name
        ];

        Teacher::updateOrCreate(['id' => $request->teacher_id], $arr_data);

        if ($request->teacher_id == "") {
            $message = "Inserted";
        } else {
            $message = "Updated";
        }
        return redirect()->back()->with('message', '<div class="alert alert-success alert-dismissible">Teacher record has been ' . $message . '</div>');
    }

    public function getdata(Request $request)
    {
        $data = Teacher::find($request->id);
        return response()->json([
            'data' => $data
        ]);
    }

    public function destroy(Request $request)
    {
        $teacher = Teacher::find($request->teacher_id);
        if ($teacher->delete()) {
            return redirect()->back()->with('message', '<div class="alert alert-info alert-dismissible">Teacher has been Deleted</div>');
        }
    }
}
