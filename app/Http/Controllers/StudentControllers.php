<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentControllers extends Controller
{
    public function index()
    {
        $students = Student::with('room')->paginate(10);

        $rooms = Room::all();
        $arr_rooms = ['' => '== Choose Room =='];
        foreach ($rooms as $val) {
            $arr_rooms[$val->id] = $val->name;
        }

        return view('student.index', ['students' => $students, 'arr_rooms' => $arr_rooms]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string'
        ]);

        $arr_data = [
            'name'    => $request->name,
            'room_id' => $request->room_id
        ];

        Student::updateOrCreate(['id' => $request->student_id], $arr_data);

        if ($request->student_id == "") {
            $message = "Inserted";
        } else {
            $message = "Updated";
        }
        return redirect()->back()->with('message', '<div class="alert alert-success alert-dismissible">Student record has been ' . $message . '</div>');
    }

    public function getdata(Request $request)
    {
        $data = Student::find($request->id);
        return response()->json([
            'data' => $data
        ]);
    }

    public function destroy(Request $request)
    {
        $room = Student::find($request->room_id);
        if ($room->delete()) {
            return redirect()->back()->with('message', '<div class="alert alert-info alert-dismissible">Student has been Deleted</div>');
        }
    }
}
