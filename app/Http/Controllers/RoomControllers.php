<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use PDF;

class RoomControllers extends Controller
{
    public function index()
    {
        $rooms = Room::paginate(10);

        $teachers = Teacher::all();
        $arr_teachers = ['' => '== Choose Teacher =='];
        foreach ($teachers as $val) {
            $arr_teachers[$val->id] = $val->name;
        }

        return view('room.index', ['rooms' => $rooms, 'arr_teachers' => $arr_teachers]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'teacher_id' => 'required'
        ]);

        $arr_data = [
            'name'       => $request->name,
            'teacher_id' => $request->teacher_id
        ];

        Room::updateOrCreate(['id' => $request->room_id], $arr_data);

        if ($request->room_id == "") {
            $message = "Inserted";
        } else {
            $message = "Updated";
        }
        return redirect()->back()->with('message', '<div class="alert alert-success alert-dismissible">Room record has been ' . $message . '</div>');
    }

    public function view($id)
    {
        $room = Room::find($id);
        $students = Student::where('room_id', null)->select('id', 'name')->get();

        return view('room.view', [
            'room' => $room,
            'students' => $students
        ]);
    }

    public function downloads()
    {
        $rooms = Room::all();

        $pdf = PDF::loadView('room.downloads', compact('rooms'));
        return $pdf->download('downloads.pdf');
    }

    public function download($id)
    {
        $rooms = Room::find($id);

        $pdf = PDF::loadView('room.download', compact('rooms'));
        return $pdf->download('download.pdf');
    }

    public function assign(Request $request)
    {
        $student = Student::find($request->student_id);
        $student->room_id = $request->room_id;
        $student->save();

        if ($request->room_id == "") {
            $message = '<div class="alert alert-info alert-dismissible">Student ' . $student->name . ' deleted in List</div>';
        } else {
            $message = '<div class="alert alert-success alert-dismissible">Student ' . $student->name . ' has been Assign</div>';
        }

        return redirect()->back()->with('message', $message);
    }

    public function getdata(Request $request)
    {
        $data = Room::find($request->id);
        return response()->json([
            'data' => $data
        ]);
    }

    public function destroy(Request $request)
    {
        $room = Room::find($request->room_id);
        if ($room->delete()) {
            return redirect()->back()->with('message', '<div class="alert alert-info alert-dismissible">Room has been Deleted</div>');
        }
    }
}
