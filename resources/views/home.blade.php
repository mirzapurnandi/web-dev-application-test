@extends('layouts.app')

@section('title', ' Beranda ')

@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Beranda</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item active">Beranda</li>
            </ol>
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Welcome to Application</h5>
                    </div>
                    <div class="card-body text-center">
                        <h1>Selamat datang di aplikasi Pengelola Sekolah <br><b>SMK Negeri 1 Blangpidie.</b><br><br></h1>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
