@extends('layouts.app')

@section('title', ' Student Index ')

@section('header')
    <style>
        nav svg{
            height: 20px;
        }
    </style>
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Students</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item active">Student Index</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Data Student
                            <button type="button" class="btn btn-success btn-add">
                                <i class="nav-icon fas fa-plus"></i> Add
                            </button>
                        </h5>
                    </div>
                    <div class="card-body">
                        @if(Session::has('message'))
                            {!! Session::get('message') !!}
                        @endif
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">No.</th>
                                    <th>Name</th>
                                    <th>Room</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($students as $student)
                                <tr>
                                    <td>{{ $student->id }}</td>
                                    <td>{{ $student->name }}</td>
                                    <td>{{ $student->room == null ? " - " : $student->room->name }}</td>
                                    <td style="display: flex;">
                                        <a href="#" class="btn btn-success btn-sm btn-edit" key="{{ $student->id }}">
                                            <i class="nav-icon fas fa-edit"></i> Edit
                                        </a> |
                                        <form action="{{ route('student.destroy') }}" method="POST" class="form-horizontal">
                                            @csrf
                                            <input type="hidden" name="student_id" value="{{ $student->id }}">
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Delete Student... ?')"> <i class="nav-icon fas fa-trash"></i> Delete </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        {{ $students->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-add">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add new Student</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('student.store') }}">
                    @csrf
                    <input type="hidden" name="student_id" id="student_id" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" value="{{ old('name') }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" placeholder="Enter new Student">
                            @if($errors->has('name'))
                                <code>{{ $errors->first('name') }}</code>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! form_dropdown('room_id', $arr_rooms, '', 'class="form-control" id="room_id"') !!}
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script type="text/javascript">
         $(document).ready(function() {
            @if($errors->first('name')) {
                $("#modal-add").modal('show');
            }
            @endif

            $(".btn-add").click(function(){
                $(".modal-title").html('Add new Student');
                $("#modal-add").modal('show');
                $("#student_id").val('');
                $("#name").val('');
                $("#teacher_id").val('');
            });

            $(".btn-edit").click(function(){
                var roomid = $(this).attr('key');
                caridata(roomid);
            });
        });

        function caridata(id){
			$.ajax({
				url: "/students-getdata/" + id,
				type: "GET",
				data: { id : id },
				dataType: "json",
				cache: false,
				success: function(response){
                    $("#student_id").val(response.data.id);
                    $("#name").val(response.data.name);
                    $("#room_id").val(response.data.room_id);

                    $("#modal-add").modal('show');
                    $(".modal-title").html('Edit Student');
				}
			});
		}
    </script>
@endpush
