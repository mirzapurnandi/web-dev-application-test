@extends('layouts.app')

@section('title', ' Teacher Index ')

@section('header')
    <style>
        nav svg{
            height: 20px;
        }
    </style>
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Teachers</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item active">Teacher Index</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Data Teacher
                            <button type="button" class="btn btn-success btn-add">
                                <i class="nav-icon fas fa-plus"></i> Add
                            </button>
                        </h5>
                    </div>
                    <div class="card-body">
                        @if(Session::has('message'))
                            {!! Session::get('message') !!}
                        @endif
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">No.</th>
                                    <th>Name</th>
                                    <th>Class / Room</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($teachers as $teacher)
                                <tr>
                                    <td>{{ $teacher->id }}</td>
                                    <td>{{ $teacher->name }}</td>
                                    <td>
                                        @if($teacher->room->count() > 0)
                                            <ol>
                                                @foreach ($teacher->room as $val)
                                                    <li><a href="{{ route('room.view', $val->id) }}">{{ $val->name }}</a></li>
                                                @endforeach
                                            </ol>
                                        @endif
                                    </td>
                                    <td style="display: flex;">
                                        <a href="#" class="btn btn-success btn-sm btn-edit" key="{{ $teacher->id }}">
                                            <i class="nav-icon fas fa-edit"></i> Edit
                                        </a> |
                                        <form action="{{ route('teacher.destroy') }}" method="POST" class="form-horizontal">
                                            @csrf
                                            <input type="hidden" name="teacher_id" value="{{ $teacher->id }}">
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Delete Teacher... ?')"> <i class="nav-icon fas fa-trash"></i> Delete </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        {{ $teachers->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-add">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add new Teacher</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('teacher.store') }}">
                    @csrf
                    <input type="hidden" name="teacher_id" id="teacher_id" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" value="{{ old('name') }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" placeholder="Enter new Teacher">
                            @if($errors->has('name'))
                                <code>{{ $errors->first('name') }}</code>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script type="text/javascript">
         $(document).ready(function() {
            @if($errors->first('name') || $errors->first('teacher_id')) {
                $("#modal-add").modal('show');
            }
            @endif

            $(".btn-add").click(function(){
                $(".modal-title").html('Add new Teacher');
                $("#modal-add").modal('show');
                $("#teacher_id").val('');
                $("#name").val('');
            });

            $(".btn-edit").click(function(){
                var id = $(this).attr('key');
                caridata(id);
            });
        });

        function caridata(id){
			$.ajax({
				url: "/teachers-getdata/" + id,
				type: "GET",
				data: { id : id },
				dataType: "json",
				cache: false,
				success: function(response){
                    $("#teacher_id").val(response.data.id);
                    $("#name").val(response.data.name);

                    $("#modal-add").modal('show');
                    $(".modal-title").html('Edit Teacher');
				}
			});
		}
    </script>
@endpush
