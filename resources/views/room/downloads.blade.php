<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download Room</title>
    <style>
        #tab{
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
            border-collapse: collapse;
            width: 100%;
        }

        #tab td, #tab th{
            border: 1px solid #ddd;
            padding: 8px;
        }

        #tab tr:nth-child(even){
            background-color: rgb(150, 94, 94);
        }

        #tab th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: aqua;
            color: black;
        }

        h1{
            text-align: center;
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
        }
    </style>
</head>
<body>
    <h1>List Room</h1>
    <table id="tab">
        <thead>
            <tr>
                <th>No</th>
                <th>Class Name</th>
                <th>Teacher Name</th>
                <th>Total Students</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rooms as $key => $room)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $room->name }}</td>
                    <td>{{ $room->teacher->name }}</td>
                    <td>{{ $room->student->count() }} Student</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
