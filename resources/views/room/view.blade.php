@extends('layouts.app')

@section('title', ' Rooms View ')

@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Data {{ $room->name }}</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('room.index') }}">Room</a>
                </li>
                <li class="breadcrumb-item active">Room View</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Teacher: <b>{{ $room->teacher->name }}</b></h5>
                    </div>
                    <div class="card-body">
                        @if(Session::has('message'))
                            {!! Session::get('message') !!}
                        @endif
                        <div class="row">
                            <div class="col-lg-6">
                                <h3>Students not Registered</h3>
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">No.</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($students as $key => $val)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $val->name }}</td>
                                            <td>
                                                <form action="{{ route('room.assign') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="room_id" value="{{ $room->id }}">
                                                    <input type="hidden" name="student_id" value="{{ $val->id }}">
                                                    <button type="submit" class="btn btn-outline-danger btn-sm" onclick="return confirm('Assign Student... ?')"> Assign <i class="fas fa-arrow-circle-right"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if($students->count() == 0)
                                            <tr>
                                                <td colspan="3" class="text-center">:: Data Kosong ::</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <h3>List Students</h3>
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">No.</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($room->student as $key => $val)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $val->name }}</td>
                                            <td>
                                                <form action="{{ route('room.assign') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="room_id" value="">
                                                    <input type="hidden" name="student_id" value="{{ $val->id }}">
                                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Delete Student... ?')"> Delete <i class="fas fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if($room->student->count() == 0)
                                            <tr>
                                                <td colspan="3" class="text-center">:: Data Kosong ::</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                <a href="{{ route('room.download', $room->id) }}" class="btn btn-info btn-block btn-flat"><i class="fa fa-download"></i> Download </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
