<?php

namespace Database\Seeders;

use App\Models\Room;
use Faker\Factory as faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $room = collect(Room::all()->modelKeys());
        $faker = Faker::create();

        foreach (range(1, 100) as $index) {
            $nomor = $index++;
            DB::table('students')->insert([
                'name' => $faker->name,
                'room_id' => $room->random(),
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ]);
        }
    }
}
