<?php

namespace Database\Seeders;

use App\Models\Teacher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher = collect(Teacher::all()->modelKeys());
        foreach (range(1, 30) as $index) {
            $nomor = $index++;
            DB::table('rooms')->insert([
                'name' => 'Room ' . $nomor,
                'teacher_id' => $teacher->random(),
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ]);
        }
    }
}
